from django.urls import path
from .views import index, hobby

urlpatterns = [
    path('', index, name='index'),
    path('hobby', hobby, name='hobby'),
]