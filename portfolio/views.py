from django.shortcuts import render

def index(request):
    return render(request, 'myweb/index.html')

def hobby(request):
    return render(request, 'myweb/hobby.html')
