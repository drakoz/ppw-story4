from django import forms
from .models import Schedule

class Schedule_Form(forms.ModelForm):
    # matkul = forms.CharField(label='Matkul', required=True,
    #     max_length=40,
    #     widget=forms.TextInput)
    # pengajar = forms.CharField(label='Dosen Pengajar', required=True,
    #     max_length=40,
    #     widget=forms.TextInput)
    # sks = forms.IntegerField(label='Jumlah SKS', required=True)
    # desc = forms.CharField(label='Tambahkan deskripsi jika diperlukan', required=False, widget=forms.TextInput)
    # term = forms.CharField(label='Term', required=True,
    #     max_length=10, widget=forms.TextInput)
    # ruangan = forms.CharField(label='Ruangan', required=True,
    #     max_length=16, widget=forms.Textarea)
    class Meta:
        model = Schedule
        fields = '__all__'