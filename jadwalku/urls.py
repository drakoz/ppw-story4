from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('tambah', views.message_post),
    path('<int:num>', views.matkul),
    path('delete/<int:num>', views.delete)
]