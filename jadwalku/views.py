from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import Schedule_Form
from .models import Schedule, Assignment
from datetime import datetime, timedelta
from time import mktime
import pytz

def index(request):
    items = Schedule.objects.all()
    context = {'items':items}
    return render(request, 'jadwalku/jadwalfull.html', context)

def matkul(request, num):
    items = Schedule.objects.get(id=num)
    tugas = Assignment.objects.filter(tugas = items)
    context = {'items':items, 'tugas':tugas}
    deadlinelist = []
    deadlinetype = []
    for tugass in context['tugas']:
        deadlinelist.append(tugass.deadline)
        if pytz.utc.localize(datetime.now()) > tugass.deadline:
            deadlinetype.append('belumlewat')
        else:
            deadlinetype.append('sudahlewat')
    context['deadlinelist'] = deadlinelist
    context['deadlinetype'] = deadlinetype
    context['pointer'] = 0

    return render(request, 'jadwalku/perjadwal.html', context)

def message_post(request):
    if request.method == 'POST':
        form = Schedule_Form(request.POST)
        if form.is_valid():
            form.save()
            html = 'schedule.html'
            return redirect('' + '/schedule')
        else:
            return redirect('' + '/schedule')
    else:
        return render(request, 'jadwalku/schedule.html')

def delete(request, num):
    items = Schedule.objects.get(id=num)
    items.delete()
    return redirect('' + '/schedule')
