from django.contrib import admin
from .models import Schedule,  Assignment

# Register your models here.
admin.site.register(Schedule)
admin.site.register(Assignment)