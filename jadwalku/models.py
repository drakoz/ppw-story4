from django.db import models
from django.forms import ModelForm

class Schedule(models.Model):
    matkul = models.CharField(max_length=40)
    pengajar = models.CharField(max_length=40)
    sks = models.IntegerField()
    desc = models.CharField(max_length=1000)
    term = models.CharField(max_length=10)
    ruangan = models.CharField(max_length=16)

    def __str__(self):
        return self.matkul

class Assignment(models.Model):
    tugas = models.ForeignKey(Schedule, on_delete=models.CASCADE)
    deadline =  models.DateTimeField()
    judul_tugas = models.CharField(max_length=25, default="")

    def __str__(self):
        return self.judul_tugas

    class Meta:
        ordering = ['deadline']