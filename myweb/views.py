from django.shortcuts import render
from datetime import datetime, timedelta

def index(request):
    return render(request, 'myweb/home.html')

def time(request):
    return render(request, 'myweb/time.html', {'time': datetime.now() + timedelta(hours=7)})

def timezoned(request, zone):
    return render(request, 'myweb/time.html', {'zona' : datetime.now() + timedelta(hours=int(zone)+7)})